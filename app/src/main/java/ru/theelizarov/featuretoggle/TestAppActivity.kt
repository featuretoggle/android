package ru.theelizarov.featuretoggle

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.theelizarov.featuretoggle.library.TestController


/**
 * Created by  Sergey Elizarov (elizarov1988@gmail.com)
 * on 07/11/2018 12:19
 */
class TestAppActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TestController();
    }
}