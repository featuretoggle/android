# FeatureToggle

Библиотека, позволяющая интегрировать подход включение/отключение фитч приложения/сервиса без комментирования кусков кода.

Библиотека опирается на **FeatureToggleProperties**, в котором содержится набор флажков доступности фитч

Библиотека предоставляет декларативный стиль для интеграции фитч

## Пример использования

Предположим, у нас есть некая фитча, реализуемая методом:

``` java
fun feature1() {
    //do somethins
}
```

Библиотека предоставляет возможность определить доступность фитчи в приложении следующим образом:

``` java
@FeatureApply(
    FeatureToggleProperties = FeatureToggleProperties::class,
    FeatureTogglePropertiesProvider = FeatureProvider.LOCAL | FeatureProvider.SERVICE,
    FeatureTogglePropertiesPrimary = FeatureProvider.LOCAL | FeatureProvider.SERVICE,
    flag = "isEnableFeature1"
    onNotEnable = IgroreFeatureContoller::class
)
fun feature1() {
    //do somethins
}
```

Аннотация @FeatureApply позволяет настроить логику включения/отключения фитчи, в аннотации можно указать класс флажков фитч, флаг фитчи и обработчик, 
который будет срабатывать, в случае, если фитча отключна. В качестве обработчика можно выбрать один из предустановленных обработчиков.

Определив аннотацию, во всех местах проекта, где используется данный метод будет добавлен код:

``` java
if (FeatureToggleProperties.isEnableFeature) feature1()
else  IgnoreFeatureController().use()
```
 